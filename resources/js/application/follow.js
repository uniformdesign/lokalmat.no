var textFollowTrue = "Følger";
var textFollowTrueHover = "Ikke følg";
var textFollowFalse = "Følg";

var followClass = "is-following";
var followClassTemp = "is-following-temp";

$(".logged-in .button.follow").hover(

  function(){
    if ($(this).hasClass(followClass)) {
      $(this).html(textFollowTrueHover);
    }
  },
  function (){
    if ($(this).hasClass(followClass)) {
      $(this).html(textFollowTrue);
    }
  }

);

$(".logged-in .button.follow").click(function(){

  if ($(this).hasClass(followClass)) {
    $(this).html(textFollowFalse);
    $(this).removeClass(followClass);
  } else {
    $(this).html(textFollowTrue);
    $(this).addClass(followClass);

    $(this).addClass(followClassTemp);

    $(this).mouseleave(function(){
      $(this).removeClass(followClassTemp);
    })

  }

});
