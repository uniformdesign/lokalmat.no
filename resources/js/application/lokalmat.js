var popupOptions = {
    //'minWidth': '150',
    'closeButton': false
};

$('body.product').ready(function() {

    //Mapbox Access Token
    L.mapbox.accessToken = 'pk.eyJ1IjoibWhhYWdlbnMiLCJhIjoiaTRCRHBUcyJ9.yi72HWaoyfyk7WUhhtsgVQ';

    // Instansier kart og sett koordinater for utsnitt
    var map = L.mapbox.map('map', 'mapbox.light').setView([59.5475789, 11.366460299999972], 14);

    // Lag EN markør og legg til i kartet
    var marker = L.marker([59.5475789, 11.366460299999972]).addTo(map);

    // Sett stil for markøren
    marker.setIcon(L.divIcon({
        className: 'markercontainer', // Make sure you set an icon class here, otherwise default styles will be set by Mapbox's CSS
        html: '<span><span class="marker"</span></span>', // The content of your HTML marker, you can build a string based on the marker properties by using 'feature.properties.your_property'
        iconSize: [10, 10] // The bounds for your icon
    }));

    // Skru av scrollweel-zooming
    map.scrollWheelZoom.disable();

    // Gjem zoom-kontroll
    $('.leaflet-top').hide();

});


$('body.producer').ready(function() {

    //Mapbox Access Token
    L.mapbox.accessToken = 'pk.eyJ1IjoibWhhYWdlbnMiLCJhIjoiaTRCRHBUcyJ9.yi72HWaoyfyk7WUhhtsgVQ';

    // Instansier kart og sett koordinater for utsnitt
    var map = L.mapbox.map('map', 'mapbox.light').setView([59.5475789, 11.366460299999972], 5);

    // Lag EN markør og legg til i kartet
    var marker = L.marker([59.5475789, 11.366460299999972]).addTo(map);

    // Sett stil for markøren
    marker.setIcon(L.divIcon({
        className: 'markercontainer', // Make sure you set an icon class here, otherwise default styles will be set by Mapbox's CSS
        html: '<span><span class="marker"</span></span>', // The content of your HTML marker, you can build a string based on the marker properties by using 'feature.properties.your_property'
        iconSize: [10, 10] // The bounds for your icon
    }));

    // Skru av scrollweel-zooming
    map.scrollWheelZoom.disable();

    // Gjem zoom-kontroll
    $('.leaflet-top').hide();
});

$('body.search').ready(function() {


    // Sett opp kart

    L.mapbox.accessToken = 'pk.eyJ1IjoibWhhYWdlbnMiLCJhIjoiaTRCRHBUcyJ9.yi72HWaoyfyk7WUhhtsgVQ';
    var map = L.mapbox.map('map', 'mapbox.light')


    map.scrollWheelZoom.disable();

    var listings = document.getElementById('listings');
    var locations = L.mapbox.featureLayer().addTo(map);

    locations.on('layeradd', function(e) {
        var marker = e.layer;
        var feature = marker.feature;

        // marker.setIcon(L.divIcon({
        //     className: 'markercontainer',
        //     iconSize: [10, 10]
        // }));
        marker.setIcon(L.divIcon({
            className: 'markercontainer',
            html: '<span class="marker"></span>',
            iconSize: [10, 10]
        }));

        var firstproducer = geojson[0].properties.producer.title;

        var moreThanOneEqual = 0;
        var onlyOneProducer = false;

        $.each(geojson, function(i, item) {
           if (item.properties.producer.title === firstproducer) {
            moreThanOneEqual++;
           }
        });​

        if (moreThanOneEqual === geojson.length) {
            onlyOneProducer = true;
        }

        if (onlyOneProducer) {
            setTimeout(function() {
                 map.setView(marker.getLatLng(), 5);
            }, 0);
        } else {
            setTimeout(function() {
                map.fitBounds(locations.getBounds());
            }, 0);
        }
    });

    locations.setGeoJSON(geojson);

    locations.eachLayer(function(locale) {

        var prop = locale.feature.properties;

        var shortDesc = prop.producer.description;
        var shortDesc = jQuery.trim(shortDesc).substring(0, 70).trim(this) + "...";

        var popup =
            '<div class="c-list-item-wrap">' +
            '<div class="producer">' +
            '<div class="c-producer-info">' +
            '<div class="m-profilepic is-lifted small" style="background-image: url(' + prop.producer.logo + ')"><\/div>' +
            '<h3>' + prop.producer.title + '<\/h3>' +
            '<a class="c-producer-link" href="' + prop.producer.link + '">Gå til produsent<\/a>' +
            '<\/div>' +
            '<\/div>' +
            '<\/div>';

        var listing = listings.appendChild(document.createElement('div'));
        var categoryprop = prop.category;
        var countyprop = prop.producer.fylke;
        var seasonprops = prop.sesong.join(' ');
        var merkeordningprops = prop.merkeordning.join(' ');

        listing.className = 'c-list-item-wrap mix ' + categoryprop + ' ' + countyprop + ' ' + seasonprops + ' ' + merkeordningprops + '';
        if (prop.isFollowing === true) {
            $(listing).addClass('isfollowing');
        }
        if (prop.epd.length > 0) {
            $(listing).addClass('epd-true');
        }
        if (prop.grossist === true) {
            $(listing).addClass('grossist-true');
        }
        if (prop.direktedistribuert === true) {
            $(listing).addClass('direktedistribuert-true');
        }

        var product = listing.appendChild(document.createElement('div'));
        product.className = 'c-product is-animated';

        var productimagecontainer = product.appendChild(document.createElement('div'));
        productimagecontainer.className = 'c-product-image';
        var imagesource = prop.media;
        $(productimagecontainer).attr("style", "background-image:url(" + imagesource + ")");

        if (prop.liked === true) {
            var likedbutton = productimagecontainer.appendChild(document.createElement('button'));
            likedbutton.className = 'button heart active';
        } else {
            var likebutton = productimagecontainer.appendChild(document.createElement('button'));
            likebutton.className = 'button heart';
        }

        var productinfo = product.appendChild(document.createElement('div'));
        productinfo.className = 'c-product-info';

        var producer = productinfo.appendChild(document.createElement('p'));
        producer.className = 'c-product-info_producer';
        producer.innerHTML = prop.producer.title;

        var title = productinfo.appendChild(document.createElement('h3'));
        title.className = 'c-product-info_title';
        title.innerHTML = prop.title;

        var productdescription = productinfo.appendChild(document.createElement('div'));
        productdescription.className = 'c-product-info_desc';
        productdescription.innerHTML = prop.description;

        var producttags = product.appendChild(document.createElement('ul'));
        producttags.className = 'c-product-info_tags inline-list';

        prop.tags.forEach(function(entry) {
            var producttag = producttags.appendChild(document.createElement('li'));
            var producttaglink = producttag.appendChild(document.createElement('a'));
            producttaglink.className = 'label product';
            producttaglink.innerHTML = entry;
        });

        var productlink = product.appendChild(document.createElement('a'));
        productlink.className = 'c-product-link';
        $(productlink).attr("href", prop.link);
        productlink.innerHTML = 'Gå til produkt';

        product.onmouseover = function() {
            locale.openPopup();
            locale.setIcon(L.divIcon({
                className: 'markercontainer',
                html: '<span class="marker active"></span>',
                iconSize: [10, 10]
            }));
            return false;
        };

        product.onmouseout = function() {

            locale.closePopup();
            //map.fitBounds(locations.getBounds());
            locale.setIcon(L.divIcon({
                className: 'markercontainer',
                html: '<span class="marker"></span>',
                iconSize: [10, 10]
            }));
            return false;
        };

        popup += '</div>';
        locale.bindPopup(popup, popupOptions);

    });


    // Mixitup

    $(".filter").on("click", function(e) {
        e.preventDefault();
    });

    // Filter

    $(function() {

        var checkboxFilter = {

            $filters: null,
            $reset: null,
            groups: [],
            outputArray: [],
            outputString: '',

            init: function() {
                var self = this;

                self.$filters = $('#Filters');
                self.$reset = $('#Reset');
                self.$container = $('#listings');

                self.$filters.find('fieldset').each(function() {
                    self.groups.push({
                        $inputs: $(this).find('input'),
                        active: [],
                        tracker: false
                    });
                });

                self.bindHandlers();
            },

            bindHandlers: function() {
                var self = this;

                self.$filters.on('change', function() {
                    self.parseFilters();
                    console.log(self.outputArray);
                });

                self.$reset.on('click', function(e) {
                    e.preventDefault();
                    self.$filters[0].reset();
                    self.parseFilters();
                });
            },

            parseFilters: function() {
                var self = this;

                for (var i = 0, group; group = self.groups[i]; i++) {
                    group.active = []; // reset arrays
                    group.$inputs.each(function() {
                        $(this).is(':checked') && group.active.push(this.value);
                    });
                    group.active.length && (group.tracker = 0);
                }

                self.concatenate();
            },

            concatenate: function() {
                var self = this,
                    cache = '',
                    crawled = false,
                    checkTrackers = function() {
                        var done = 0;

                        for (var i = 0, group; group = self.groups[i]; i++) {
                            (group.tracker === false) && done++;
                        }

                        return (done < self.groups.length);
                    },
                    crawl = function() {
                        for (var i = 0, group; group = self.groups[i]; i++) {
                            group.active[group.tracker] && (cache += group.active[group.tracker]);

                            if (i === self.groups.length - 1) {
                                self.outputArray.push(cache);
                                cache = '';
                                updateTrackers();
                            }
                        }
                    },
                    updateTrackers = function() {
                        for (var i = self.groups.length - 1; i > -1; i--) {
                            var group = self.groups[i];

                            if (group.active[group.tracker + 1]) {
                                group.tracker++;
                                break;
                            } else if (i > 0) {
                                group.tracker && (group.tracker = 0);
                            } else {
                                crawled = true;
                            }
                        }
                    };

                self.outputArray = [];

                do {
                    crawl();
                }
                while (!crawled && checkTrackers());

                self.outputString = self.outputArray.join();

                !self.outputString.length && (self.outputString = 'all');

                if (self.$container.mixItUp('isLoaded')) {
                    self.$container.mixItUp('filter', self.outputString);
                }
            }
        };

        // Tags og dropdown-listing av filtre

        var selectedCategoriesList = [];
        $("ul#Kategorier").on('click', 'input:checkbox', function() {
            var tag = $(this).attr("id");
            var tagtext = String(tag).replace(/-/g, ' ');
            var taginlist = $('span#' + tag + '')
            if (this.checked) {
                $("#tagslist > fieldset").append("<span id='" + tag + "' class='label filter " + tag + "'>" + tagtext + "<button tabindex='0' class='close' aria-label='Fjern filter' data-close='" + tag + "'>&times;</button></span>");
                selectedCategoriesList.push(tagtext);
                $("#KategorierDropdownBtn").html(selectedCategoriesList.toString());
            } else {
                selectedCategoriesList.splice(selectedCategoriesList.indexOf(tag), 1);
                taginlist.remove();
                if (selectedCategoriesList.length === 0) {
                    $("#KategorierDropdownBtn").html('Kategorier');
                } else {
                    $("#KategorierDropdownBtn").html(selectedCategoriesList.toString());
                }
            }
        });

        var selectedCountiesList = [];
        $("ul#Fylker").on('click', 'input:checkbox', function() {
            var tag = $(this).attr("id");
            var tagtext = String(tag).replace(/-/g, ' ');
            var taginlist = $('span#' + tag + '')
            if (this.checked) {
                $("#tagslist > fieldset").append("<span id='" + tag + "' class='label filter " + tag + "'>" + tag + "<button tabindex='0' class='close' aria-label='Fjern filter' data-close='" + tag + "'>&times;</button></span>");
                selectedCountiesList.push(tagtext);
                $("#FylkerDropdownBtn").html(selectedCountiesList.toString());
            } else {
                selectedCountiesList.splice(selectedCountiesList.indexOf(tag), 1);
                taginlist.remove();
                if (selectedCountiesList.length === 0) {
                    $("#FylkerDropdownBtn").html('Fylker');
                } else {
                    $("#FylkerDropdownBtn").html(selectedCountiesList.toString());
                }
            }
        });

        // Klikke på tag for å slette

        $(document).on('click', "span.label", function() {
            var tagid = $(this).attr("id");
            var checkbox = $('input:checkbox[id^="' + tagid + '"]:checked')
            $(checkbox).trigger("click");
            this.remove();
        });

        $(function() {

            checkboxFilter.init();

            $('#listings').mixItUp({
                load: {
                    page: 1,
                },
                pagination: {
                    limit: 6, // antall treff pr side
                    prevButtonHTML: '←',
                    nextButtonHTML: '→',
                    maxPagers: 4
                },
                animation: {
                    enable: false
                },
                selectors: {
                    pagersWrapper: '.pagination'
                },
                callbacks: {
                    onMixFail: function(state) {
                        console.log('Ingen resultat for ' + state.activeFilter + '');
                    },
                    onMixLoad: function(state) {
                        //$('<li class="unavailable" aria-disabled="true"><a href="">&hellip;</a></li>').insertBefore('li.page-next');
                        //$('<li><a href="">'+state.totalPages+'</a></li>').insertBefore('li.page-next');
                    },
                    onMixEnd: function(state) {}
                }
            });

        });


    });


});
