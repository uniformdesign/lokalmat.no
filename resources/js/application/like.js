$(document).ready(function() {
  $(document).on('click', '.heart', function() {
    if ($("body").hasClass("logged-in")) {
      if ($(this).hasClass("active")) {
        $( this ).removeClass( "active-animation" );
      } else {
        $( this ).addClass( "active-animation" );
      };
      $( this ).toggleClass( "active" );
    } else {
      $('#modal-login-from-lik').foundation('reveal', 'open');
    }​
  });
});


var textLikeTrue = "Lagret";
var textLikeTrueHover = "Slett";
var textLikeFalse = "Lagre";

var likeClass = "is-liking";
var likeClassTemp = "is-liking-temp";

$(".logged-in .button.like").hover(

  function(){
    if ($(this).hasClass(likeClass)) {
      $(this).html(textLikeTrueHover);
    }
  },
  function (){
    if ($(this).hasClass(likeClass)) {
      $(this).html(textLikeTrue);
    }
  }

);

$(".logged-in .button.like").click(function(){

  if ($(this).hasClass(likeClass)) {
    $(this).html(textLikeFalse);
    $(this).removeClass(likeClass);
  } else {
    $(this).html(textLikeTrue);
    $(this).addClass(likeClass);

    $(this).addClass(likeClassTemp);

    $(this).mouseleave(function(){
      $(this).removeClass(likeClassTemp);
    })

  }

});
