module.exports = {
	options: {
		assets: '<%= paths.dev %>',
		data: '<%= paths.src %>/templates/data/**/*.json',
		helpers: '<%= paths.src %>/templates/helpers/**/*.js',
		layoutdir: '<%= paths.src %>/templates/layouts/',
		plugins: [ 'assemble-related-pages' ],
		layout: false,
		partials: [
			'<%= paths.src %>/templates/partials/**/*.hbs',
			'<%= paths.src %>/templates/layouts/*.hbs'
		],
		collections: [
			{
				name: 'product'
			},
			{
				name: 'producer'
			},
			{
				name: 'sitemap'
			},
			{
				name: 'innkjoper-nav'
			},
			{
				name: 'nav-produsent'
			},
			{
				name: 'nav-prod-rediger-produkt'
			},
			{
				name: 'nav-prod-rediger-profil'
			},
			{
				name: 'nav-innkjoper-rediger-profil'
			},
			{
				name: 'article'
			},
			{
				name: 'following-producer'
			},
			{
				name: 'following-product'
			},
			{
				name: 'i-mitt-sortiment'
			},
			{
				name: 'my-product'
			},
			{
				name: 'soketreff'
			},
			{
				name: 'new-producer'
			}
		]
	},
	utlogget: {
		options: {
			plugins: ['assemble-contrib-permalinks','assemble-contrib-sitemap','assemble-related-pages'],
			mode: 'utlogget'
		},
		files: [{
			cwd: '<%= paths.src %>/templates/pages/',
			dest: '<%= paths.dev %>/',
			expand: true,
			flatten: true,
			src: ['**/*.hbs']
		}]
	},
	innkjoper: {
		options: {
			plugins: ['assemble-contrib-permalinks','assemble-contrib-sitemap','assemble-related-pages'],
			mode: 'innkjoper'
		},
		files: [{
			cwd: '<%= paths.src %>/templates/pages/',
			dest: '<%= paths.dev %>/innkjoper',
			expand: true,
			flatten: true,
			src: ['**/*.hbs']
		}]
	},
	produsent: {
		options: {
			plugins: ['assemble-contrib-permalinks','assemble-contrib-sitemap','assemble-related-pages'],
			mode: 'produsent'
		},
		files: [{
			cwd: '<%= paths.src %>/templates/pages/',
			dest: '<%= paths.dev %>/produsent',
			expand: true,
			flatten: true,
			src: ['**/*.hbs']
		}]
	},
	docs: {
		files: [
		{
			cwd: '<%= paths.src %>/templates/docs/',
			dest: '<%= paths.dev %>/docs',
			expand: true,
			flatten: false,
			src: ['**/*.hbs']
			}
		]
	}
};
