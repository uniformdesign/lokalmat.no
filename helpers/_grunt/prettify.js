module.exports = {
	options: {
	  "indent": 2,
	  "indent_char": " ",
		"condense": true,
		"padcomments": true,
	  "indent_scripts": "normal",
	  "wrap_line_length": 0,
	  "brace_style": "expand",
	  "preserve_newlines": false,
	  "max_preserve_newlines": 1,
	  "unformatted": [
	    "a",
	    "code",
	    "pre"
	  ]
	},
	all: {
    expand: true,
		flatten: false,
    cwd: '<%= paths.dev %>/',
    ext: '.html',
    src: ['**/*.html'],
    dest: '<%= paths.dev %>/'
  }
};
