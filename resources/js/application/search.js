$( "#search input" )
  .focus(function() {
    $( "body" ).addClass("is-searching");
  })
  .focusout(function() {
    $( "body" ).removeClass("is-searching");
  })
;
