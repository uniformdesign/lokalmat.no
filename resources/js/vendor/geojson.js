var geojson = [{
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [23.90883210000004,
                71.0049782
            ]
        },
        "properties": {
            "id": 2,
            "title": "Rørossmør",
            "link": "rorossmor.html",
            "description": "Økologisk Røros Smør er laget av surrømme og kjernet etter gammel industrimetode i kjeglekjerne. Smaken er syrlig, har stor smaksrikdom og innslag av vannperler.",
            "tags": [
                "Meieriprodukter",
                "Sør-Trøndelag"
            ],
            "merkeordning": [],
            "category": "Meieriprodukter",
            "sesong": [
                "januar",
                "februar"
            ],
            "isFollowing": false,
            "liked": true,
            "epd": "",
            "grossist": false,
            "direktedistribuert": true,
            "media": "/media/rorosmeieriet/smor2.jpg",
            "producer": {
                "title": "Rørosmeieriet",
                "description": "Rørosmeieriet tilbyr økologisk mat av høy kvalitet gjennom industrialisert håndverksproduksjon, med utspring i rike mattradisjoner og gode råvarer fra Rørostraktene.",
                "fylke": "Sør-Trøndelag",
                "logo": "/media/rorosmeieriet/profile-rorosmeieriet.jpg",
                "media": "/media/rorosmeieriet/roros1.jpg",
                "link": "/innkjoper/rorosmeieriet.html",
                "following": true
            }

        }
    },
{
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [23.90883210000004,
                71.0049782
            ]
        },
        "properties": {
            "id": 2,
            "title": "Rørossmør",
            "link": "rorossmor.html",
            "description": "Økologisk Røros Smør er laget av surrømme og kjernet etter gammel industrimetode i kjeglekjerne. Smaken er syrlig, har stor smaksrikdom og innslag av vannperler.",
            "tags": [
                "Meieriprodukter",
                "Sør-Trøndelag"
            ],
            "merkeordning": [],
            "category": "Meieriprodukter",
            "sesong": [
                "januar",
                "februar"
            ],
            "isFollowing": false,
            "liked": true,
            "epd": "",
            "grossist": false,
            "direktedistribuert": true,
            "media": "/media/rorosmeieriet/smor2.jpg",
            "producer": {
                "title": "Rørosmeieriet",
                "description": "Rørosmeieriet tilbyr økologisk mat av høy kvalitet gjennom industrialisert håndverksproduksjon, med utspring i rike mattradisjoner og gode råvarer fra Rørostraktene.",
                "fylke": "Sør-Trøndelag",
                "logo": "/media/rorosmeieriet/profile-rorosmeieriet.jpg",
                "media": "/media/rorosmeieriet/roros1.jpg",
                "link": "/innkjoper/rorosmeieriet.html",
                "following": true
            }

        }
    }
];
