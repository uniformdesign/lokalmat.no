module.exports = {
	options: {
		compress: {
			global_defs: {
				"DEBUG": false
			},
			dead_code: true
		},
		sourceMap: true,
		mangle: false,
		beautify: true
	},
	app: {
		files: {
			'<%= paths.dev %>/js/app.js': [

				// Fastclick
				'resources/bower-components/foundation/js/vendor/fastclick.js',

				// Foundation
				'resources/bower-components/foundation/js/foundation/foundation.js',
				'resources/bower-components/foundation/js/foundation/foundation.topbar.js',
				'resources/bower-components/foundation/js/foundation/foundation.tab.js',
				'resources/bower-components/foundation/js/foundation/foundation.tooltip.js',
				'resources/bower-components/foundation/js/foundation/foundation.dropdown.js',
				'resources/bower-components/foundation/js/foundation/foundation.abide.js',
				'resources/bower-components/foundation/js/foundation/foundation.reveal.js',
				'resources/bower-components/foundation/js/foundation/foundation.clearing.js',
				'resources/bower-components/foundation/js/foundation/foundation.accordion.js',

				//Select2
				'resources/bower-components/select2/dist/js/select2.min.js',

				//Mixitup
				'resources/bower-components/mixitup/build/jquery.mixitup.min.js',

				//Readyselector
				'resources/bower-components/jquery-readyselector/jquery.readyselector.js',

				// Other
				'<%= paths.src %>/js/**/*.js',

				// Except modernizr.js
				'!<%= paths.src %>/js/vendor/modernizr.js'

			]
		}
	}
};
